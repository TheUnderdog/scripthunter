import nltk

nltk.download('punkt')
nltk.download('averaged_perceptron_tagger')

def ExtractDescriptiveKeywords(IncomingSentence):
	TempTokens = nltk.word_tokenize(IncomingSentence)
	TempTagged = nltk.pos_tag(TempTokens)

	DescriptiveWords = [word for word,pos in TempTagged \
							if (pos == 'NN' or pos == 'NNP' or pos == 'NNS' or pos == 'NNPS')]
				
	return DescriptiveWords


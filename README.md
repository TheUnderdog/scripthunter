#Comment to stop GitLab deleting this stuff


# ScriptHunter

Tool for parsing a video script and hunting down public domain/creative common images that match.

# Requirements

* Requires KeywordExtractor (included)

* Requires nltk [Natural Language Toolkit]:

`pip3 install nltk`

* Requires CCAPI

`https://gitlab.com/TheUnderdog/ccapi`

* CCAPI requires the following libraries are installed (please refer to CCAPI for up-to-date instructions):

* Pillow

`pip3 install Pillow`

* html2text

`pip3 install html2text`

* pdf2image

`pip3 install pdf2image`

* xmltodict

`pip3 install xmltodict`

* urllib

`pip3 install urllib`

[Note: urllib may already be included as part of other Python libraries and may not require installation]

# Example Command Line Call:

`python3 ScriptHunter.py -s Testing this sentence for any cool descriptive words like running. -l 1 -w 1048 -r False`

* -s stands for 'sentence' (required)
* -l stands for 'limit' how many images to download per keyword found (default: 1)
* -w stands for 'width' which is the pixel width the image will be resized to (default: None)
* -r stands for 'rename' which will rename the file to search term (note: will overwrite if more than 1 image downloaded per keyword; default False)
* -o stands for 'output' which is the target folder to save in (default: the Images folder that is in the same directory as ScriptHunter.py)


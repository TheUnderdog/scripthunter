#!/usr/bin/env python3

#version 1.1

import requests
import sys
import os
import argparse
import subprocess
from WikimediaDownloader import *
from FileIOA import *

def CreateFolder(FullPath):
	if not os.path.exists(FullPath):
		os.makedirs(FullPath)
	
def CheckFileForURL(TargetURL,Filename):
	Lines = LoadFileLines(Filename,False)
	
	for Line in Lines:	
		if TargetURL in Line:
			return True
			
	return False
	
def AddURLToFile(TargetURL,Filename):
	AppendFileLine(Filename,TargetURL)

def MakeWikimediaCommonsRequest(SearchTerm,Limit,Width):
	TargetUrl = (
	"https://en.wikipedia.org/w/api.php"
	"?action=query"
	"&generator=images"
	"&prop=imageinfo"
	"&gimlimit="+str(Limit)+
	"&redirects=1"
	"&list=search"
	"&srsearch="+urllib.parse.quote(SearchTerm)+
	"&srnamespace=6"
	"&iiprop="+"|".join(["timestamp","user","userid","canonicaltitle","url","size","mime"])+
	"&iiurlwidth="+str(Width)+
	"&format=json")

	return requests.get(TargetUrl).json()

def DownloadImage(TargetFile,TargetOutput,Width=None):
	
	TargetFileExt = TargetFile[TargetFile.rfind('.')+1:].lower()

	if "png" in TargetFileExt or "jpg" in TargetFileExt or "bmp" in TargetFileExt or "pdf" in TargetFileExt:	
		DownloadWikimediaImage(TargetFile,TargetOutput,Width)


def CCAPIFrontEnd(*,SearchTerm,Limit=1,Width=None,Output=".",MustRename=False):
	CreateFolder(Output)
	
	Results = MakeWikimediaCommonsRequest(SearchTerm,Limit,Width)
	IterLimit = 0
	
	for Entry in Results["query"]["search"]:
		TargetFile = Entry['title'].replace("File:","")
		
		try:
			if not FileExists(TargetFile):
				DownloadImage(TargetFile,Output,Width)
				
				if MustRename:
					TempFile = TargetFile.strip().replace(' ', '_')
					
					FileExt = ExtractEXT(TempFile)
					FileName = SearchTerm+"."+FileExt
					RenameFile(os.path.join(Output,TempFile),os.path.join(Output,FileName))
		
		except Exception as e:
			raise Exception(e)
			
		IterLimit += 1
		if IterLimit == Limit:
			break

if __name__ == "__main__":	
	
	CLIParser = argparse.ArgumentParser(description="Download images from Wikimedia Commons based on search terms.")	
	CLIParser.add_argument("-s", "--search", help="The search terms to use",required=True,nargs="*")
	CLIParser.add_argument("-l", "--limit", help="The limit of images to download (default=1)",default=1,type=int)
	CLIParser.add_argument("-w", "--width", help="The image width of the image to download in pixels (default=None)",default=None,type=int)
	CLIParser.add_argument("-o", "--output", help="The directory to download the images to (default=.)",default=".")
	CLIParser.add_argument("-r", "--rename", help="Whether to rename the images to the search terms (default=False)",default=False,type=bool)
	
	CLIArgs = CLIParser.parse_args()
	
	CCAPIFrontEnd(
					SearchTerm = " ".join(CLIArgs.search),
					Limit = CLIArgs.limit,
					Width = CLIArgs.width,
					Output = CLIArgs.output,
					MustRename = CLIArgs.rename
					)
	
	


# CCAPI

Creative Commons API Image Downloader tool, written in Python.

Takes a search term, and downloads images or files relating (if the file is a PDF, it automatically converts all of it's pages to an image).

**Python Library Requirements**

Must be run in Python3

Must have the following libraries installed:

* Pillow

`pip3 install Pillow`

* html2text

`pip3 install html2text`

* pdf2image

`pip3 install pdf2image`

* xmltodict

`pip3 install xmltodict`

* urllib

`pip3 install urllib`

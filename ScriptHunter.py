
from KeywordExtractor import *

import os, sys, inspect

ImportFolder = os.path.realpath(os.path.join(os.path.dirname(inspect.getfile(inspect.currentframe())),"CCAPI"))
if ImportFolder not in sys.path:
	sys.path.insert(0, ImportFolder)

from CCAPI import *

def ScriptHunterFrontEnd(*,Sentence,Limit=1,Width=None,Output=".",MustRename=False):
	Keywords = ExtractDescriptiveKeywords(Sentence)
	
	for EachKeyword in Keywords:
		try:
			CCAPIFrontEnd(SearchTerm=EachKeyword,Limit=1,Width=Width,Output=Output,MustRename=MustRename)
			
		except Exception as e:
			raise Exception(e)

if __name__ == "__main__":
	CLIParser = argparse.ArgumentParser(description="Download images from Wikimedia Commons based on search terms.")	
	CLIParser.add_argument("-s", "--sentence", help="The sentence to parse",required=True,nargs="*")
	CLIParser.add_argument("-l", "--limit", help="The limit of images to download per keyword (default=1)",default=1,type=int)
	CLIParser.add_argument("-w", "--width", help="The image width of the image to download in pixels (default=None)",default=None,type=int)
	CLIParser.add_argument("-o", "--output", help="The directory to download the images to (default=.)",default="./Images")
	CLIParser.add_argument("-r", "--rename", help="Whether to rename the images to the search terms (default=False)",default=False,type=bool)
	
	CLIArgs = CLIParser.parse_args()
	
	ScriptHunterFrontEnd(
					Sentence = " ".join(CLIArgs.sentence),
					Limit = CLIArgs.limit,
					Width = CLIArgs.width,
					Output = CLIArgs.output,
					MustRename = CLIArgs.rename
					)
